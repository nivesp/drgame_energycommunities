''' 
=========================================================================
Script         : Input data
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
'''

# Load packages
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D  
import tikzplotlib

import numpy as np
from collections import OrderedDict 

from datetime import datetime
import pandas as pd

from OptProblem import OptProblem

from random import gauss



def plot_increasing_AmbAver(case,costData):

	rangeStudy = costData.index.get_level_values(0).tolist()
	
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	#
	ax1.plot(rangeStudy, costData['Cost_Ar'].tolist(), '-s',color='r',label='Retailer')
	ax1.fill_between(rangeStudy, costData['StdLB_Ar'].tolist(), costData['StdUB_Ar'].tolist(),
		color='red',alpha=0.5,label='_nolegend_')
	#
	ax1.plot(rangeStudy, costData['Cost_Error'].tolist(), '-s',color='orange',label='Net load deviation')
	ax1.fill_between(rangeStudy, costData['StdLB_Error'].tolist(), costData['StdUB_Error'].tolist(),
		color='orange',alpha=0.5,label='_nolegend_')
	#
	ax1.plot(rangeStudy, costData['Cost_Pros1'].tolist(), '-o',color='green',label='Prosumer1')
	ax1.fill_between(rangeStudy, costData['StdLB_Pros1'].tolist(), costData['StdUB_Pros1'].tolist(),
		color='green',alpha=0.5,label='_nolegend_')
	#
	ax1.plot(rangeStudy, costData['Cost_Pros2'].tolist(), '-^',color='blue',label='Prosumer2')
	ax1.fill_between(rangeStudy, costData['StdLB_Pros2'].tolist(), costData['StdUB_Pros2'].tolist(),
		color='blue',alpha=0.5,label='_nolegend_')
	#
	# ax1.plot(rangeStudy,payoff['Tot'],color='black',label='Community welfare')
	# ax1.fill_between(rangeStudy,var_lb['Tot'], var_ub['Tot'],
		# color='black',alpha=0.1,label='_nolegend_')
	#
	ax1.set_title(case)
	ax1.set_xlabel('Ambiguity aversion [-]')
	ax1.set_ylabel('Disbenefit [EUR]')
	ax1.set_xlim(min(rangeStudy),max(rangeStudy))
	ax1.legend()
	#
	tikzplotlib.save('./data/output/graphics/%s_IncRho_cost_%s.tex' 
		% (datetime.today().strftime('%Y%m%d'),case),
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_IncRho_cost_%s.pdf' 
		% (datetime.today().strftime('%Y%m%d'),case),
		format='pdf', dpi=1000)	  
		


def plot_E(case,costData):

	rangeStudy = costData.index.get_level_values(0).tolist()
	
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	ax1.plot(rangeStudy, costData['p'], '-s',color='r',label='Arbitrageur')
	ax1.plot(rangeStudy ,costData['d_Pros1'], '-o',color='green',label='Prosumer1')
	ax1.plot(rangeStudy, costData['d_Pros2'], '-^',color='blue',label='Prosumer2')
	ax1.plot(rangeStudy[0],[0],color='black',label='Energy price')
	
	ax1.set_title(case)
	ax1.set_ylabel('Power [kW]')
	ax1.set_xlabel('Ambiguity aversion [-]')
	ax1.set_xlim(min(rangeStudy),max(rangeStudy))
	ax1.legend()
	#
	# twin object for two different y-axis on the sample plot
	ax2=ax1.twinx()
	ax2.plot(rangeStudy, costData['Price_E'], color='black')
	ax2.set_ylabel('Price [EUR/kW]')
	#
	tikzplotlib.save('./data/output/graphics/%s_IncRho_E_%s.tex' 
		% (datetime.today().strftime('%Y%m%d'), case),
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_IncRho_E_%s.pdf' 
		% (datetime.today().strftime('%Y%m%d'),case),
		format='pdf', dpi=1000)


def plot_R(case,costData):

	rangeStudy = costData.index.get_level_values(0).tolist()
	
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	ax1.plot(rangeStudy, costData['alpha_Ar'], '-s',color='r',label='Arbitrageur')
	ax1.plot(rangeStudy, costData['alpha_Pros1'], '-o',color='green',label='Prosumer1')
	ax1.plot(rangeStudy, costData['alpha_Pros2'], '-^',color='blue',label='Prosumer2')
	ax1.plot(rangeStudy[0],[0],color='black',label='Reserve price')
	
	ax1.set_title(case)
	ax1.set_ylabel('Reserve participation factor [-]')
	ax1.set_xlabel('Ambiguity aversion [-]')
	ax1.set_xlim(min(rangeStudy),max(rangeStudy))
	ax1.legend()
	#
	# twin object for two different y-axis on the sample plot
	ax2=ax1.twinx()
	ax2.plot(rangeStudy, costData['Price_R'], color='black')
	ax2.set_ylabel('Price [EUR]')
	#
	tikzplotlib.save('./data/output/graphics/%s_IncRho_R_%s.tex' 
		% (datetime.today().strftime('%Y%m%d'), case),
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_IncRho_R_%s.pdf' 
		% (datetime.today().strftime('%Y%m%d'),case),
		format='pdf', dpi=1000)



def cost_3D_plot(case,costData,What):

	rangeStudyRho = costData.unstack().index.get_level_values(0).tolist()
	#rangeStudyEpsilon = costData.xs((rangeStudyRho[0]), level=('Rho')).index.tolist()
	
	X,Y = np.meshgrid(rangeStudyRho, rangeStudyRho)
	Z = costData[What].unstack().values.transpose()

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')

	ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)

	ax.set_title(case)
	ax.set_xlabel('Prosumer 1')
	ax.set_ylabel('Prosumer 2')
	ax.set_zlabel('Disbenefit [EUR]')

	ax.view_init(30, -130)
	#
	# tikzplotlib.save('./data/output/graphics/%s_RhoEpsilon_3D_%s.tex' 
		# % (datetime.today().strftime('%Y%m%d'), case),
			   # axis_height = '\\fheight',
			   # axis_width = '\\fwidth')
			   # )
	plt.savefig('./data/output/graphics/%s_RhoEpsilon_3D_%s.pdf' 
		% (datetime.today().strftime('%Y%m%d'),case),
		format='pdf', dpi=1000)
		
	# Het_Amb_1.xs((Rho_Ar_Aux), level=('Rho_Ar'))['Cost_Tot'].to_csv('./data/output/graphics/test.txt', header=True, index=True,float_format='%.2f')
		


def calc_ED_DRO(ED_DRO_Input,model_name,ED_DRO_Settings,How,ADD,
				Amb,
				rangeStudyRho,
				Rho_Ar,Rho_Pros1,Rho_Pros2):

	#########################
	# Economic Dispatch DRO #
	#########################
	model_name = 'EconomicDispatch_DRO'



	if How == 'Hom_noAmb':
	
		# RHO
		Rho_Ar = 0
		Rho_Pros1 = 0
		Rho_Pros2 = 0
		Rho_Pros = {'Pros1' : Rho_Pros1,
					'Pros2' : Rho_Pros2}
		
		# ERROR
		ErrorAr = ED_DRO_Input['Error']
		ErrorPros = { (x,y) : ED_DRO_Input['Error'][y] for x in ED_DRO_Input['Prosumers'] for y in ED_DRO_Input['Scenarios'] } 
		
		TestSet = ED_DRO_Input['Scenarios']
		ErrorTestSet = ED_DRO_Input['Error']
		
		HAr = {'b1' : max(ErrorAr.values()) + ADD, # maximum value of forecast error
			   'b2' : -1*min(ErrorAr.values()) + ADD} # minimum value of forecast error

		HPros = {}
		for n in ED_DRO_Input['Prosumers']:
			HProsAUX = {(n,'b1') : max({x : ErrorPros[n,x] for x in ED_DRO_Input['Scenarios']}.values()) + ADD,
						(n,'b2') : -1*min({x : ErrorPros[n,x] for x in ED_DRO_Input['Scenarios']}.values()) + ADD}
			HPros.update(HProsAUX)			
	
		# UPDATE
		ED_DRO_Input.update({'Rho_Ar' : Rho_Ar,
							 'Rho_Pros' : Rho_Pros,
							 #
							 'ErrorAr' : ErrorAr,
							 'ErrorPros' : ErrorPros,
							 'TestSet' : TestSet,
							 'ErrorTestSet' : ErrorTestSet,
							 'HAr': HAr,
							 'HPros' : HPros})
							 
							 
	elif How == 'Het_Amb_0':
	
		# RHO
		Rho_Pros = {'Pros1' : Rho_Pros1,
					'Pros2' : Rho_Pros2}
		
		# ERROR
		ErrorAr = ED_DRO_Input['Error']
		ErrorPros = { (x,y) : ED_DRO_Input['Error'][y] for x in ED_DRO_Input['Prosumers'] for y in ED_DRO_Input['Scenarios'] } 

		HAr = {'b1' : max(ErrorAr.values()) + ADD, # maximum value of forecast error
			   'b2' : -1*min(ErrorAr.values()) + ADD} # minimum value of forecast error

		HPros = {}
		for n in ED_DRO_Input['Prosumers']:
			HProsAUX = {(n,'b1') : max({x : ErrorPros[n,x] for x in ED_DRO_Input['Scenarios']}.values()) + ADD,
						(n,'b2') : -1*min({x : ErrorPros[n,x] for x in ED_DRO_Input['Scenarios']}.values()) + ADD}
			HPros.update(HProsAUX)
						
		# UPDATE
		ED_DRO_Input.update({'Rho_Ar' : Rho_Ar,
							 'Rho_Pros' : Rho_Pros,
							 #
							 'ErrorAr' : ErrorAr,
							 'ErrorPros' : ErrorPros,
							 'HAr': HAr,
							 'HPros' : HPros})
				
				
	elif How == 'Het_Amb_1':
	
		# RHO
		Rho_Pros = {'Pros1' : Rho_Pros1,
					'Pros2' : Rho_Pros2}
	
		# UPDATE
		ED_DRO_Input.update({'Rho_Ar' : Rho_Ar,
							 'Rho_Pros' : Rho_Pros})



	# Initialize model and run Optimization
	ED_DRO = OptProblem(model_name,ED_DRO_Input,ED_DRO_Settings) 
	ED_DRO.run_opt()
	ED_DRO.get_results(ED_DRO_Settings)
	
	
	# Collect results #
	###################
	Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'Price_E'] = ED_DRO.lambda_E
	Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'p'] = ED_DRO.p
	for n in ED_DRO_Input['Prosumers']:
		Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'d_%s' %n] = ED_DRO.d[n]
	
	Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'Price_R'] = ED_DRO.lambda_R
	Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'alpha_Ar'] = ED_DRO.alpha_Ar
	for n in ED_DRO_Input['Prosumers']:
		Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'alpha_%s' %n] = ED_DRO.alpha_Pros[n]
	
	#Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'ErrorMax_Ar'] = (ED_DRO.P_ub - ED_DRO.p)/ED_DRO.alpha_Ar
	#for n in ED_DRO_Input['Prosumers']:
	#	Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2,sigma_Ar,sigma_Pros1,sigma_Pros2),'ErrorMax_%s' %n] = (ED_DRO.D_ub[n] - ED_DRO.d[n])/ED_DRO.alpha_Pros[n]
	
	for n in ['Ar','Error','Pros1','Pros2','Tot']:
		Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'Cost_%s' %n] = ED_DRO.Obj_expected[n]['Cost']
		Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'StdLB_%s' %n] = (ED_DRO.Obj_scenario.mean() - ED_DRO.Obj_scenario.std())[n]
		Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'StdUB_%s' %n] = (ED_DRO.Obj_scenario.mean() + ED_DRO.Obj_scenario.std())[n]
		
	for n in ['Ar','Pros1','Pros2']:
		Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'CC_LB_%s' %n] = ED_DRO.ChanceConstraints[n]['LB']
		Amb.loc[(Rho_Ar,Rho_Pros1,Rho_Pros2),'CC_UB_%s' %n] = ED_DRO.ChanceConstraints[n]['UB']
	
	return Amb
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
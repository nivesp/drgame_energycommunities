''' 
=========================================================================
Script         : Opt problems: Rn, Ra-fc, Ra-fi
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
''' 

from gurobipy import * # Gurobi
import pandas as pd    # Pandas
from collections import OrderedDict 

class OptProblem():
    def __init__(self,Name,Input,Settings):
        self.m = Model(Name)
        self.create_sets_parameters(Input)
        self.create_variables(Settings)
        self.create_objective(Settings)
        self.create_constraints(Settings)


    def run_opt(self):
        self.m.Params.OptimalityTol = 1e-09
        self.m.Params.FeasibilityTol = 1e-09
        #self.m.Params.method = 2
        self.m.Params.BarQCPConvTol = 1e-15
        self.m.Params.BarConvTol = 1e-15
        self.m.Params.QCPDual = 1e-15
        #self.m.Params.NonConvex = 2
        self.m.update()
        #self.m.write('%s.lp' % self.m.model_name)
        print(self.m.modelname)
        self.m.optimize()
        if self.m.status == GRB.Status.OPTIMAL:
            print('\n OPTIMAL SOLUTION FOUND \n')
        #else:
        #    sys.exit('\n ERROR: NO SOLUTION FOUND \n')


    def create_sets_parameters(self,Input):
    # Sets #
    ########
        if 'Scenarios' in Input.keys():
            self.Scenarios = Input['Scenarios']
        if 'Prosumers' in Input.keys():
            self.Prosumers = Input['Prosumers']

    # Parameters #
    ##############
        if 'Cost' in Input.keys():
            self.Cost = Input['Cost']
        if 'Utility' in Input.keys():
            self.Utility = Input['Utility']
		#
        if 'Beta' in Input.keys():
            self.Beta = Input['Beta']
		#
        if 'TestSet' in Input.keys():
            self.TestSet = Input['TestSet']
        if 'ErrorTestSet' in Input.keys():
            self.ErrorTestSet = Input['ErrorTestSet']
        if 'ProbTestSet' in Input.keys():
            self.ProbTestSet = Input['ProbTestSet']
		#
        if 'ErrorTEST' in Input.keys():
            self.ErrorTEST = Input['ErrorTEST']
        if 'Error' in Input.keys():
            self.Error = Input['Error']
        if 'ErrorAr' in Input.keys():
            self.ErrorAr = Input['ErrorAr']
        if 'ErrorPros' in Input.keys():
            self.ErrorPros = Input['ErrorPros']
        if 'Prob' in Input.keys():
            self.Prob = Input['Prob']
        if 'Delta' in Input.keys():
            self.Delta = Input['Delta']
		#
        if 'P_lb' in Input.keys():
            self.P_lb = Input['P_lb']
        if 'P_ub' in Input.keys():
            self.P_ub = Input['P_ub']
		#
        if 'D_lb' in Input.keys():
            self.D_lb = Input['D_lb']
        if 'D_ub' in Input.keys():
            self.D_ub = Input['D_ub']
		
		#######
		# DRO #
		#######
        if 'Rho_Ar' in Input.keys():
            self.Rho_Ar = Input['Rho_Ar']
        if 'Rho_Pros' in Input.keys():
            self.Rho_Pros = Input['Rho_Pros']
        if 'Epsilon' in Input.keys():
            self.Epsilon = Input['Epsilon']
		#
        if 'Bounds' in Input.keys():
            self.Bounds = Input['Bounds']
        if 'Q' in Input.keys():
            self.Q = Input['Q']
        if 'HAr' in Input.keys():
            self.HAr = Input['HAr']
        if 'HPros' in Input.keys():
            self.HPros = Input['HPros']
		#
        if 'Price_E' in Input.keys():
            self.Price_E = Input['Price_E']
        if 'Price_R' in Input.keys():
            self.Price_R = Input['Price_R']


    def create_variables(self,Settings):
    # Variables #
    #############
        if 'p' in Settings['variables']:
            self.p = self.m.addVar(
            lb=-GRB.INFINITY, ub=GRB.INFINITY, name='p')
        if 'd' in Settings['variables']:
            self.d = self.m.addVars(
            self.Prosumers, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='d')
		#
        if 'alpha_Ar' in Settings['variables']:
            self.alpha_Ar = self.m.addVar(
            lb=-GRB.INFINITY, ub=GRB.INFINITY, name='alpha_Ar')
        if 'alpha_Pros' in Settings['variables']:
            self.alpha_Pros = self.m.addVars(
            self.Prosumers, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='alpha_Pros')
		#
		#######
		# DRO #
		#######
        if 'Var_Obj' in Settings['variables']:
			# Arbitrageur
            self.lambdaDRO_Ar = self.m.addVar(
            lb=0, ub=GRB.INFINITY, name='lambdaDRO_Ar')
			
            self.lambdaDRO_AUX_Ar = self.m.addVars(
            self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambdaDRO_AUX_Ar')

            self.sigma_Ar = self.m.addVars(
            self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='sigma_Ar')
			
            self.gamma_Ar = self.m.addVars(
            self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma_Ar')
			
			# Prosumer
            self.lambdaDRO_Pros = self.m.addVars(
            self.Prosumers, lb=0, ub=GRB.INFINITY, name='lambdaDRO_Pros')
			
            self.lambdaDRO_AUX_Pros = self.m.addVars(
            self.Prosumers, self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambdaDRO_AUX_Pros')

            self.sigma_Pros = self.m.addVars(
            self.Prosumers, self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='sigma_Pros')
			
            self.gamma_Pros = self.m.addVars(
            self.Prosumers, self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma_Pros')
		#
        if 'Var_d_lb_DRCC' in Settings['variables']:
            self.tau_Pros_lb = self.m.addVars(
            self.Prosumers, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='tau_Pros_lb')

            self.lambda_Pros_lb = self.m.addVars(
            self.Prosumers, lb=0, ub=GRB.INFINITY, name='lambda_Pros_lb')

            self.lambda_Pros_lb_AUX1 = self.m.addVars(
            self.Prosumers, self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambda_Pros_lb_AUX1')

            self.lambda_Pros_lb_AUX2 = self.m.addVars(
            self.Prosumers, self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambda_Pros_lb_AUX2')

            self.sigma_Pros_lb = self.m.addVars(
            self.Prosumers, self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='sigma_Pros_lb')
			
            self.gamma1_Pros_lb = self.m.addVars(
            self.Prosumers, self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma1_Pros_lb')
			
            self.gamma2_Pros_lb = self.m.addVars(
            self.Prosumers, self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma2_Pros_lb')
		#
        if 'Var_d_ub_DRCC' in Settings['variables']:
            self.tau_Pros_ub = self.m.addVars(
            self.Prosumers, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='tau_Pros_ub')

            self.lambda_Pros_ub = self.m.addVars(
            self.Prosumers, lb=0, ub=GRB.INFINITY, name='lambda_Pros_ub')

            self.lambda_Pros_ub_AUX1 = self.m.addVars(
            self.Prosumers, self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambda_Pros_ub_AUX1')

            self.lambda_Pros_ub_AUX2 = self.m.addVars(
            self.Prosumers, self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambda_Pros_ub_AUX2')
			
            self.sigma_Pros_ub = self.m.addVars(
            self.Prosumers, self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='sigma_Pros_ub')
			
            self.gamma1_Pros_ub = self.m.addVars(
            self.Prosumers, self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma1_Pros_ub')
			
            self.gamma2_Pros_ub = self.m.addVars(
            self.Prosumers, self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma2_Pros_ub')
		#
        if 'Var_p_lb_DRCC' in Settings['variables']:
            self.tau_Ar_lb = self.m.addVar(
            lb=-GRB.INFINITY, ub=GRB.INFINITY, name='tau_Ar_lb')

            self.lambda_Ar_lb = self.m.addVar(
            lb=0, ub=GRB.INFINITY, name='lambda_Ar_lb')

            self.lambda_Ar_lb_AUX1 = self.m.addVars(
            self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambda_Ar_lb_AUX1')

            self.lambda_Ar_lb_AUX2 = self.m.addVars(
            self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambda_Ar_lb_AUX2')

            self.sigma_Ar_lb = self.m.addVars(
            self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='sigma_Ar_lb')
			
            self.gamma1_Ar_lb = self.m.addVars(
            self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma1_Ar_lb')
			
            self.gamma2_Ar_lb = self.m.addVars(
            self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma2_Ar_lb')
		#
        if 'Var_p_ub_DRCC' in Settings['variables']:
            self.tau_Ar_ub = self.m.addVar(
            lb=-GRB.INFINITY, ub=GRB.INFINITY, name='tau_Ar_ub')

            self.lambda_Ar_ub = self.m.addVar(
            lb=0, ub=GRB.INFINITY, name='lambda_Ar_ub')

            self.lambda_Ar_ub_AUX1 = self.m.addVars(
            self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambda_Ar_ub_AUX1')

            self.lambda_Ar_ub_AUX2 = self.m.addVars(
            self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='lambda_Ar_ub_AUX2')

            self.sigma_Ar_ub = self.m.addVars(
            self.Scenarios, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='sigma_Ar_ub')		
			
            self.gamma1_Ar_ub = self.m.addVars(
            self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma1_Ar_ub')
			
            self.gamma2_Ar_ub = self.m.addVars(
            self.Bounds, self.Scenarios, lb=0, ub=GRB.INFINITY, name='gamma2_Ar_ub')	
			

    def create_objective(self,Settings):
	
		#######
		# SAA #
		#######	
        if 'CostMin_CP_SAA' in Settings['objective']:
            self.Obj = self.m.setObjective(
			#
			self.Cost*self.p - quicksum( self.Utility[n]*self.d[n] for n in self.Prosumers )
			#
            - quicksum( self.Prob[w]*( self.Cost*self.alpha_Ar*self.Error[w] + quicksum( self.Utility[n]*self.alpha_Pros[n]*self.Error[w] for n in self.Prosumers ) ) for w in self.Scenarios )
			#
			# Regularizer
			+ quicksum( (1/2)*self.Beta*self.alpha_Pros[n]*self.alpha_Pros[n] + (1/2)*self.Beta*self.d[n]*self.d[n] for n in self.Prosumers )
			+ (1/2)*self.Beta*self.alpha_Ar*self.alpha_Ar + (1/2)*self.Beta*self.p*self.p
			#
            , GRB.MINIMIZE )
			

		#######
		# DRO #
		#######			
        if 'CostMin_CP_DRO' in Settings['objective']:
            self.Obj = self.m.setObjective(
			#
			quicksum( - self.Utility[n]*self.d[n] + self.lambdaDRO_Pros[n]*self.Rho_Pros[n] + quicksum( self.Prob[i]*self.sigma_Pros[n,i] for i in self.Scenarios ) for n in self.Prosumers )
			#
			+ self.Cost*self.p + self.lambdaDRO_Ar*self.Rho_Ar + quicksum( self.Prob[i]*self.sigma_Ar[i] for i in self.Scenarios )
			#
			# Regularizer
			+ quicksum( (1/2)*self.Beta*self.alpha_Pros[n]*self.alpha_Pros[n] + (1/2)*self.Beta*self.d[n]*self.d[n] for n in self.Prosumers )
			+ (1/2)*self.Beta*self.alpha_Ar*self.alpha_Ar + (1/2)*self.Beta*self.p*self.p
			#
            , GRB.MINIMIZE )


    def create_constraints(self,Settings):

        # DA balance
        if 'EQ_balance_E' in Settings['constraints']:
            self.EQ_balance_E = self.m.addConstr(
			self.p - quicksum( self.d[n] for n in self.Prosumers ) - self.Delta == 0
			, name='EQ_balance_E')

        # Reserve balance
        if 'EQ_balance_R' in Settings['constraints']:
            self.EQ_balance_R = self.m.addConstr(
			self.alpha_Ar + quicksum( self.alpha_Pros[n] for n in self.Prosumers ) - 1 == 0
			, name='EQ_balance_R')


		#######
		# SAA #
		#######			
		
        # d lower bound
        if 'EQ_d_lb' in Settings['constraints']:
            self.EQ_d_lb = self.m.addConstrs(
			( self.D_lb[n] <= self.d[n] + self.alpha_Pros[n]*self.Error[w]
			for n in self.Prosumers for w in self.Scenarios), name='EQ_d_lb')	

        # d upper bound
        if 'EQ_d_ub' in Settings['constraints']:
            self.EQ_d_ub = self.m.addConstrs(
			( self.d[n] + self.alpha_Pros[n]*self.Error[w] <= self.D_ub[n]
			for n in self.Prosumers for w in self.Scenarios), name='EQ_d_ub')				
			
        # p lower bound
        if 'EQ_p_lb' in Settings['constraints']:
            self.EQ_p_lb = self.m.addConstrs(
			( self.P_lb <= self.p + self.alpha_Ar*self.Error[w]
			for w in self.Scenarios), name='EQ_p_lb')	

        # p upper bound
        if 'EQ_p_ub' in Settings['constraints']:
            self.EQ_p_ub = self.m.addConstrs(
			( self.p + self.alpha_Ar*self.Error[w] <= self.P_ub
			for w in self.Scenarios), name='EQ_p_ub')


		#######
		# DRO #
		#######	
		
        # DRO Obj Prosumer
        if 'EQ_Prosumer_Obj_DRO' in Settings['constraints']:
		
            self.EQ_Obj_DRO1 = self.m.addConstrs(
			( + self.Utility[n]*self.alpha_Pros[n]*self.ErrorPros[n,i] + quicksum( self.gamma_Pros[n,b,i]*( self.HPros[n,b] - self.Q[b]*self.ErrorPros[n,i] ) for b in self.Bounds )
			<= self.sigma_Pros[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_Obj_DRO1')
			
            self.EQ_Obj_DRO2a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma_Pros[n,b,i] for b in self.Bounds ) - self.Utility[n]*self.alpha_Pros[n]
			<= self.lambdaDRO_AUX_Pros[n,i] 
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_Obj_DRO2a')

            self.EQ_Obj_DRO2b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma_Pros[n,b,i] for b in self.Bounds ) + self.Utility[n]*self.alpha_Pros[n] )
			<= self.lambdaDRO_AUX_Pros[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_Obj_DRO2b')

            self.EQ_Obj_DRO2c = self.m.addConstrs(
			( self.lambdaDRO_AUX_Pros[n,i]
			<= self.lambdaDRO_Pros[n] 
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_Obj_DRO2c')


        # DRO Obj Arbitrageur
        if 'EQ_Arbitrageur_Obj_DRO' in Settings['constraints']:
		
            self.EQ_Obj_DRO1 = self.m.addConstrs(
			( + self.Cost*self.alpha_Ar*self.ErrorAr[i] + quicksum( self.gamma_Ar[b,i]*( self.HAr[b] - self.Q[b]*self.ErrorAr[i] ) for b in self.Bounds )
			<= self.sigma_Ar[i]
			for i in self.Scenarios ), name='EQ_Obj_DRO1')
			
            self.EQ_Obj_DRO2a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma_Ar[b,i] for b in self.Bounds ) + self.Cost*self.alpha_Ar 
			<= self.lambdaDRO_AUX_Ar[i]
			for i in self.Scenarios ), name='EQ_Obj_DRO2a')				

            self.EQ_Obj_DRO2b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma_Ar[b,i] for b in self.Bounds ) - self.Cost*self.alpha_Ar )
			<= self.lambdaDRO_AUX_Ar[i]
			for i in self.Scenarios ), name='EQ_Obj_DRO2b')

            self.EQ_Obj_DRO2c = self.m.addConstrs(
			( self.lambdaDRO_AUX_Ar[i] 
			<= self.lambdaDRO_Ar 
			for i in self.Scenarios ), name='EQ_Obj_DRO2c')			


		# d lower bound CVaR CC
        if 'EQ_d_lb_DRCC' in Settings['constraints']:
		
            self.EQ_d_lb_DRCC1 = self.m.addConstrs(
			( self.tau_Pros_lb[n] + ( 1/self.Epsilon )*( self.lambda_Pros_lb[n]*self.Rho_Pros[n] + quicksum( self.Prob[i]*self.sigma_Pros_lb[n,i] for i in self.Scenarios ) ) 
			<= 0 
			for n in self.Prosumers ), name='EQ_d_lb_DRCC1')
			
            self.EQ_d_lb_DRCC2 = self.m.addConstrs(
			( self.D_lb[n] - self.d[n] + self.alpha_Pros[n]*self.ErrorPros[n,i] - self.tau_Pros_lb[n] + quicksum( self.gamma1_Pros_lb[n,b,i]*( self.HPros[n,b] - self.Q[b]*self.ErrorPros[n,i] ) for b in self.Bounds )
			<= self.sigma_Pros_lb[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_lb_DRCC2')

            self.EQ_d_lb_DRCC3 = self.m.addConstrs(
			( quicksum( self.gamma2_Pros_lb[n,b,i]*( self.HPros[n,b] - self.Q[b]*self.ErrorPros[n,i] ) for b in self.Bounds )
			<= self.sigma_Pros_lb[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_lb_DRCC3')
			
            self.EQ_d_lb_DRCC4a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma1_Pros_lb[n,b,i] for b in self.Bounds ) - self.alpha_Pros[n] 
			<= self.lambda_Pros_lb_AUX1[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_lb_DRCC4a')

            self.EQ_d_lb_DRCC4b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma1_Pros_lb[n,b,i] for b in self.Bounds ) - self.alpha_Pros[n] )
			<= self.lambda_Pros_lb_AUX1[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_lb_DRCC4b')
			
            self.EQ_d_lb_DRCC4c = self.m.addConstrs(
			( self.lambda_Pros_lb_AUX1[n,i] 
			<= self.lambda_Pros_lb[n]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_lb_DRCC4c')
			
            self.EQ_d_lb_DRCC5a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma2_Pros_lb[n,b,i] for b in self.Bounds ) 
			<= self.lambda_Pros_lb_AUX2[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_lb_DRCC5a')		
			
            self.EQ_d_lb_DRCC5b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma2_Pros_lb[n,b,i] for b in self.Bounds ) )
			<= self.lambda_Pros_lb_AUX2[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_lb_DRCC5b')	
			
            self.EQ_d_lb_DRCC5c = self.m.addConstrs(
			( self.lambda_Pros_lb_AUX2[n,i] 
			<= self.lambda_Pros_lb[n]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_lb_DRCC5c')		


		# d upper bound CVaR CC
        if 'EQ_d_ub_DRCC' in Settings['constraints']:
		
            self.EQ_d_ub_DRCC1 = self.m.addConstrs(
			( self.tau_Pros_ub[n] + ( 1/self.Epsilon )*( self.lambda_Pros_ub[n]*self.Rho_Pros[n] + quicksum( self.Prob[i]*self.sigma_Pros_ub[n,i] for i in self.Scenarios) ) 
			<= 0 
			for n in self.Prosumers ), name='EQ_d_ub_DRCC1')
			
            self.EQ_d_ub_DRCC2 = self.m.addConstrs(
			( self.d[n] - self.alpha_Pros[n]*self.ErrorPros[n,i] - self.D_ub[n] - self.tau_Pros_ub[n] + quicksum( self.gamma1_Pros_ub[n,b,i]*( self.HPros[n,b] - self.Q[b]*self.ErrorPros[n,i] ) for b in self.Bounds )
			<= self.sigma_Pros_ub[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_ub_DRCC2')
			
            self.EQ_d_ub_DRCC3 = self.m.addConstrs(
			( quicksum( self.gamma2_Pros_ub[n,b,i]*( self.HPros[n,b] - self.Q[b]*self.ErrorPros[n,i] ) for b in self.Bounds )
			<= self.sigma_Pros_ub[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_ub_DRCC3')
			
            self.EQ_d_ub_DRCC4a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma1_Pros_ub[n,b,i] for b in self.Bounds ) + self.alpha_Pros[n]
			<= self.lambda_Pros_ub_AUX1[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_ub_DRCC4a')
			
            self.EQ_d_ub_DRCC4b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma1_Pros_ub[n,b,i] for b in self.Bounds ) + self.alpha_Pros[n] )
			<= self.lambda_Pros_ub_AUX1[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_ub_DRCC4b')
			
            self.EQ_d_ub_DRCC4c = self.m.addConstrs(
			( self.lambda_Pros_ub_AUX1[n,i] 
			<= self.lambda_Pros_ub[n]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_ub_DRCC4c')
			
            self.EQ_d_ub_DRCC5a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma2_Pros_ub[n,b,i] for b in self.Bounds ) 
			<= self.lambda_Pros_ub_AUX2[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_ub_DRCC5a')		
			
            self.EQ_d_ub_DRCC5b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma2_Pros_ub[n,b,i] for b in self.Bounds ) )
			<= self.lambda_Pros_ub_AUX2[n,i]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_ub_DRCC5b')	
			
            self.EQ_d_ub_DRCC5c = self.m.addConstrs(
			( self.lambda_Pros_ub_AUX2[n,i] 
			<= self.lambda_Pros_ub[n]
			for n in self.Prosumers for i in self.Scenarios ), name='EQ_d_ub_DRCC5c')		


		# p lower bound CVaR CC
        if 'EQ_p_lb_DRCC' in Settings['constraints']:
		
            self.EQ_p_lb_DRCC1 = self.m.addConstr(
			( self.tau_Ar_lb + ( 1/self.Epsilon )*( self.lambda_Ar_lb*self.Rho_Ar + quicksum( self.Prob[i]*self.sigma_Ar_lb[i] for i in self.Scenarios) ) 
			<= 0 
			), name='EQ_p_lb_DRCC1')
			
            self.EQ_p_lb_DRCC2 = self.m.addConstrs(
			( self.P_lb - self.p - self.alpha_Ar*self.ErrorAr[i] - self.tau_Ar_lb + quicksum( self.gamma1_Ar_lb[b,i]*( self.HAr[b] - self.Q[b]*self.ErrorAr[i] ) for b in self.Bounds ) 
			<= self.sigma_Ar_lb[i]
			for i in self.Scenarios ), name='EQ_p_lb_DRCC2')
			
            self.EQ_p_lb_DRCC3 = self.m.addConstrs(
			( quicksum( self.gamma2_Ar_lb[b,i]*( self.HAr[b] - self.Q[b]*self.ErrorAr[i] ) for b in self.Bounds ) 
			<= self.sigma_Ar_lb[i]
			for i in self.Scenarios ), name='EQ_p_lb_DRCC3')	
						
            self.EQ_p_lb_DRCC4a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma1_Ar_lb[b,i] for b in self.Bounds ) + self.alpha_Ar 
			<= self.lambda_Ar_lb_AUX1[i]
			for i in self.Scenarios ), name='EQ_p_lb_DRCC4a')
						
            self.EQ_p_lb_DRCC4b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma1_Ar_lb[b,i] for b in self.Bounds ) + self.alpha_Ar )
			<= self.lambda_Ar_lb_AUX1[i] 
			for i in self.Scenarios ), name='EQ_p_lb_DRCC4b')
						
            self.EQ_p_lb_DRCC4c = self.m.addConstrs(
			( self.lambda_Ar_lb_AUX1[i] 
			<= self.lambda_Ar_lb 
			for i in self.Scenarios ), name='EQ_p_lb_DRCC4c')
			
            self.EQ_p_lb_DRCC5a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma2_Ar_lb[b,i] for b in self.Bounds ) 
			<= self.lambda_Ar_lb_AUX2[i]
			for i in self.Scenarios ), name='EQ_p_lb_DRCC5a')
			
            self.EQ_p_lb_DRCC5b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma2_Ar_lb[b,i] for b in self.Bounds ) )
			<= self.lambda_Ar_lb_AUX2[i]
			for i in self.Scenarios ), name='EQ_p_lb_DRCC5b')
			
            self.EQ_p_lb_DRCC5c = self.m.addConstrs(
			( self.lambda_Ar_lb_AUX2[i] 
			<= self.lambda_Ar_lb
			for i in self.Scenarios ), name='EQ_p_lb_DRCC5c')				


		# p upper bound CVaR CC
        if 'EQ_p_ub_DRCC' in Settings['constraints']:
		
            self.EQ_p_ub_DRCC1 = self.m.addConstr(
			( self.tau_Ar_ub + ( 1/self.Epsilon )*( self.lambda_Ar_ub*self.Rho_Ar + quicksum( self.Prob[i]*self.sigma_Ar_ub[i] for i in self.Scenarios) ) 
			<= 0 
			), name='EQ_p_ub_DRCC1')
			
            self.EQ_p_ub_DRCC2 = self.m.addConstrs(
			( self.p + self.alpha_Ar*self.ErrorAr[i] - self.P_ub - self.tau_Ar_ub
			+ quicksum( self.gamma1_Ar_ub[b,i]*( self.HAr[b] - self.Q[b]*self.ErrorAr[i] ) for b in self.Bounds ) 
			<= self.sigma_Ar_ub[i]
			for i in self.Scenarios ), name='EQ_p_ub_DRCC2')
			
            self.EQ_p_ub_DRCC3 = self.m.addConstrs(
			( quicksum( self.gamma2_Ar_ub[b,i]*( self.HAr[b] - self.Q[b]*self.ErrorAr[i] ) for b in self.Bounds ) 
			<= self.sigma_Ar_ub[i]
			for i in self.Scenarios ), name='EQ_p_ub_DRCC3')			
			
            self.EQ_p_ub_DRCC4a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma1_Ar_ub[b,i] for b in self.Bounds ) - self.alpha_Ar 
			<= self.lambda_Ar_ub_AUX1[i] 
			for i in self.Scenarios ), name='EQ_p_ub_DRCC4a')				
			
            self.EQ_p_ub_DRCC4b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma1_Ar_ub[b,i] for b in self.Bounds ) - self.alpha_Ar )
			<= self.lambda_Ar_ub_AUX1[i]
			for i in self.Scenarios ), name='EQ_p_ub_DRCC4b')			
			
            self.EQ_p_ub_DRCC4c = self.m.addConstrs(
			( self.lambda_Ar_ub_AUX1[i] 
			<= self.lambda_Ar_ub 
			for i in self.Scenarios ), name='EQ_p_ub_DRCC4c')	

            self.EQ_p_ub_DRCC5a = self.m.addConstrs(
			( quicksum( self.Q[b]*self.gamma2_Ar_ub[b,i] for b in self.Bounds ) 
			<= self.lambda_Ar_ub_AUX2[i]
			for i in self.Scenarios ), name='EQ_p_ub_DRCC5a')		

            self.EQ_p_ub_DRCC5b = self.m.addConstrs(
			( - ( quicksum( self.Q[b]*self.gamma2_Ar_ub[b,i] for b in self.Bounds ) )
			<= self.lambda_Ar_ub_AUX2[i]
			for i in self.Scenarios ), name='EQ_p_ub_DRCC5b')		

            self.EQ_p_ub_DRCC5c = self.m.addConstrs(
			( self.lambda_Ar_ub_AUX2[i] 
			<= self.lambda_Ar_ub
			for i in self.Scenarios ), name='EQ_p_ub_DRCC5c')				


    def get_results(self,Settings):
        if 'objective' in Settings['results']:
            self.objective = self.m.objVal

        if 'p' in Settings['results']:
            self.p = self.p.x   
			
        if 'd' in Settings['results']:
            self.d = self.m.getAttr('x',self.d)

        if 'alpha_Ar' in Settings['results']:
            self.alpha_Ar = self.alpha_Ar.x   
			
        if 'alpha_Pros' in Settings['results']:
            self.alpha_Pros = self.m.getAttr('x',self.alpha_Pros)
			
        if 'lambda_E' in Settings['results']:
            self.lambda_E = self.EQ_balance_E.pi 
			
        if 'lambda_R' in Settings['results']:
            self.lambda_R = self.EQ_balance_R.pi
			
        if 'lambda_E_DRO' in Settings['results']:
            self.lambda_E = self.EQ_balance_E.pi 
			
        if 'lambda_R_DRO' in Settings['results']:
            self.lambda_R = self.EQ_balance_R.pi
			

		# CENTRAL PLANNER with SAMPLE AVERAGE APPROXIMATION
		###################################################
        if 'ObjPros_SAA' in Settings['eval']:
            self.ObjPros = {n :
			#
			( self.lambda_E - self.Utility[n] )*self.d[n] - self.lambda_R*self.alpha_Pros[n]
			+ (1/2)*self.Beta*self.alpha_Pros[n]*self.alpha_Pros[n] + (1/2)*self.Beta*self.d[n]*self.d[n]
			- sum( self.Prob[w]*self.alpha_Pros[n]*self.Error[w]*self.Utility[n] for w in self.Scenarios )
			#
			for n in self.Prosumers}

        if 'ObjError_SAA' in Settings['eval']:
            self.ObjError = self.lambda_E*self.Delta + 1*self.lambda_R
			
        if 'ObjAr_SAA' in Settings['eval']:
            self.ObjAr = {'ar' :
			#
			( self.Cost - self.lambda_E )*self.p - self.lambda_R*self.alpha_Ar
			+ (1/2)*self.Beta*self.alpha_Ar*self.alpha_Ar + (1/2)*self.Beta*self.p*self.p
			- sum( self.Prob[w]*self.Cost*self.alpha_Ar*self.Error[w] for w in self.Scenarios )
			#
			}			
			
			
		# # CENTRAL PLANNER with DRO
		# ##########################
        # if 'CP_DRO' in Settings['eval']:
		
			# # PROSUMERS
            # self.ObjPros_scenario = OrderedDict()
            # for n in self.Prosumers:
                # for w in self.Scenarios: 
                    # self.ObjPros_scenario.update(
                    # {(n,w) : ( self.lambda_E - self.Utility[n] )*self.d[n] - self.lambda_R*self.alpha_Pros[n]
                    # + (1/2)*self.Beta*self.alpha_Pros[n]*self.alpha_Pros[n] + (1/2)*self.Beta*self.d[n]*self.d[n]
                    # #
                    # - self.alpha_Pros[n]*self.Utility[n]*self.ErrorTEST[w]
                    # #+ self.Rho_Pros[n]*self.alpha_Pros[n]*self.Utility[n] 
					# } )
					
					
            # self.ObjPros_expected = {n :
            # #
            # ( self.lambda_E - self.Utility[n] )*self.d[n] - self.lambda_R*self.alpha_Pros[n]
            # + (1/2)*self.Beta*self.alpha_Pros[n]*self.alpha_Pros[n] + (1/2)*self.Beta*self.d[n]*self.d[n]
            # #
            # - sum( self.Prob[w]*self.alpha_Pros[n]*self.Utility[n]*self.ErrorTEST[w] for w in self.Scenarios )
            # #+ self.Rho_Pros[n]*self.alpha_Pros[n]*self.Utility[n]
            # #
            # for n in self.Prosumers}           


			# # FORECAST ERROR
            # self.ObjError = self.lambda_E*self.Delta + 1*self.lambda_R
			
			
			# # ARBITRAGEUR
            # self.ObjAr_scenario = OrderedDict()
            # for w in self.Scenarios:
                # self.ObjAr_scenario.update(
                # { w : ( self.Cost - self.lambda_E )*self.p - self.lambda_R*self.alpha_Ar
                # + (1/2)*self.Beta*self.alpha_Ar*self.alpha_Ar + (1/2)*self.Beta*self.p*self.p
                # #
                # - self.Cost*self.alpha_Ar*self.ErrorTEST[w]
                # #+ self.Rho_Ar*self.Cost*self.alpha_Ar 
				# })


            # self.ObjAr_expected = ( ( self.Cost - self.lambda_E )*self.p - self.lambda_R*self.alpha_Ar
                # + (1/2)*self.Beta*self.alpha_Ar*self.alpha_Ar + (1/2)*self.Beta*self.p*self.p
                # #
                # - sum( self.Prob[w]*self.Cost*self.alpha_Ar*self.ErrorTEST[w] for w in self.Scenarios )
                # #+ self.Rho_Ar*self.Cost*self.alpha_Ar 
				# )
            

			# # OBJECTIVES TO PANDAS DATAFRAME #
			
            # # Per scenario
            # ##############
            # self.Obj_scenario = pd.DataFrame({}, index=self.Scenarios)            
            # self.Obj_scenario['Ar'] = ( [self.ObjAr_scenario[w] for w in self.Scenarios] )				
            # self.Obj_scenario['Error'] = ( [self.ObjError for w in self.Scenarios] )
            # for n in self.Prosumers:
                # self.Obj_scenario[n] = ( [self.ObjPros_scenario[n,w] for w in self.Scenarios] )
            # # total payoff per scenario
            # self.Obj_scenario['Tot'] = (self.Obj_scenario.sum(axis=1) )

            # # Expected
            # ##########
            # self.Obj_expected = pd.DataFrame( {'Ar' : [0] }, index=['Cost'] )
            # self.Obj_expected.loc['Cost','Ar'] = ( self.ObjAr_expected )
            # self.Obj_expected.loc['Cost','Error'] = ( self.ObjError )    
            # for n in self.Prosumers:
                # self.Obj_expected.loc['Cost',n] = ( self.ObjPros_expected[n] )
            # # total cost
            # self.Obj_expected.loc['Cost','Tot'] = ( self.Obj_expected.sum(axis=1)['Cost'] )
			
			
			
		# OUT OF SAMPLE #
		
		# CENTRAL PLANNER with DRO
		##########################
        if 'CP_DRO' in Settings['eval']:
		
			# PROSUMERS
            self.ObjPros_scenario = OrderedDict()
            for n in self.Prosumers:
                for w in self.TestSet: 
                    self.ObjPros_scenario.update(
                    {(n,w) : ( self.lambda_E - self.Utility[n] )*self.d[n] - self.lambda_R*self.alpha_Pros[n]
                    + (1/2)*self.Beta*self.alpha_Pros[n]*self.alpha_Pros[n] + (1/2)*self.Beta*self.d[n]*self.d[n]
                    #
                    + self.alpha_Pros[n]*self.Utility[n]*self.ErrorTestSet[w]
					} )
					
					
            self.ObjPros_expected = {n :
            #
            ( self.lambda_E - self.Utility[n] )*self.d[n] - self.lambda_R*self.alpha_Pros[n]
            + (1/2)*self.Beta*self.alpha_Pros[n]*self.alpha_Pros[n] + (1/2)*self.Beta*self.d[n]*self.d[n]
            #
            + sum( self.ProbTestSet[w]*self.alpha_Pros[n]*self.Utility[n]*self.ErrorTestSet[w] for w in self.TestSet )
            #
            for n in self.Prosumers}           


			# FORECAST ERROR
            self.ObjError = self.lambda_E*self.Delta + 1*self.lambda_R
			
			
			# ARBITRAGEUR
            self.ObjAr_scenario = OrderedDict()
            for w in self.TestSet:
                self.ObjAr_scenario.update(
                { w : ( self.Cost - self.lambda_E )*self.p - self.lambda_R*self.alpha_Ar
                + (1/2)*self.Beta*self.alpha_Ar*self.alpha_Ar + (1/2)*self.Beta*self.p*self.p
                #
                + self.Cost*self.alpha_Ar*self.ErrorTestSet[w]
				})


            self.ObjAr_expected = ( ( self.Cost - self.lambda_E )*self.p - self.lambda_R*self.alpha_Ar
                + (1/2)*self.Beta*self.alpha_Ar*self.alpha_Ar + (1/2)*self.Beta*self.p*self.p
                #
                + sum( self.ProbTestSet[w]*self.Cost*self.alpha_Ar*self.ErrorTestSet[w] for w in self.TestSet )
				)
            

			# OBJECTIVES TO PANDAS DATAFRAME #
			
            # Per scenario
            ##############
            self.Obj_scenario = pd.DataFrame({}, index=self.TestSet)            
            self.Obj_scenario['Ar'] = ( [self.ObjAr_scenario[w] for w in self.TestSet] )				
            self.Obj_scenario['Error'] = ( [self.ObjError for w in self.TestSet] )
            for n in self.Prosumers:
                self.Obj_scenario[n] = ( [self.ObjPros_scenario[n,w] for w in self.TestSet] )
				
            # total payoff per scenario
            self.Obj_scenario['Tot'] = (self.Obj_scenario.sum(axis=1) )

            # Expected
            ##########
            self.Obj_expected = pd.DataFrame( {'Ar' : [0] }, index=['Cost'] )
            self.Obj_expected.loc['Cost','Ar'] = ( self.ObjAr_expected )
            self.Obj_expected.loc['Cost','Error'] = ( self.ObjError )    
            for n in self.Prosumers:
                self.Obj_expected.loc['Cost',n] = ( self.ObjPros_expected[n] )
				
            # total cost
            self.Obj_expected.loc['Cost','Tot'] = ( self.Obj_expected.sum(axis=1)['Cost'] )
            
            
            
            # CHANCE CONSTRAINTS #
			
            self.ChanceConstraints = pd.DataFrame( {'Ar' : [0,0] }, index=['LB','UB'] )
            
            # ARBITRAGEUR	
            self.CC_lb = []		
            self.CC_ub = []
            for w in self.TestSet:            
                if self.p + self.alpha_Ar*self.ErrorTestSet[w] < self.P_lb:
                    self.CC_lb.append(1)		
                elif self.p + self.alpha_Ar*self.ErrorTestSet[w] > self.P_ub:
                    self.CC_ub.append(1)
			
            self.ChanceConstraints.loc['LB','Ar'] = (1/len(self.TestSet))*sum(self.CC_lb)
            self.ChanceConstraints.loc['UB','Ar'] = (1/len(self.TestSet))*sum(self.CC_ub)
			
            # PROSUMER			
            for n in self.Prosumers:	
                self.CC_lb = []		
                self.CC_ub = []
                
                for w in self.TestSet:			
                    if self.d[n] - self.alpha_Pros[n]*self.ErrorTestSet[w] < self.D_lb[n]:
                        self.CC_lb.append(1)		
                    elif self.d[n] - self.alpha_Pros[n]*self.ErrorTestSet[w] > self.D_ub[n]:
                        self.CC_ub.append(1)
			
                self.ChanceConstraints.loc['LB',n] = (1/len(self.TestSet))*sum(self.CC_lb)
                self.ChanceConstraints.loc['UB',n] = (1/len(self.TestSet))*sum(self.CC_ub)
			

















			
''' 
=========================================================================
Script         : Generates and solves opt problems
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
'''
# load packages
import sys

import pandas as pd
import numpy as np
import time
from datetime import datetime
import matplotlib.pylab as plt
from collections import OrderedDict 

from random import randint
from random import uniform
from random import normalvariate
from random import gauss
from random import sample
from scipy.stats import norm

# calculate in parallel
from joblib import Parallel, delayed
import multiprocessing
		
num_cores = multiprocessing.cpu_count()
#num_cores = 1

plotResults = True # plot results


#----------------------------
start = time.time()
#----------------------------

# load model settings
# load local optimization problems
exec(open('./data/input/ModelSettings.py').read())
from OptProblem import OptProblem


##############
# INPUT DATA #
##############

# Prosumers
Number_of_Prosumers = 2
Prosumers = ['Pros%d' % x for x in range(1,Number_of_Prosumers+1)]

# Scenarios
Number_of_Scenarios = 500
Scenarios = ['w%d' % x for x in range(1,Number_of_Scenarios+1)]
Prob = { x : 1/Number_of_Scenarios for x in Scenarios}


# ERROR
SampleList = []
for i in range(0,100000):
    n = np.round(normalvariate(0, 3),4)
    SampleList.append(n)

ErrorAUX = sample(SampleList, Number_of_Scenarios)	
Error = { x : ErrorAUX[Scenarios.index(x)] for x in Scenarios}
	
ErrorArAUX = sample(SampleList, Number_of_Scenarios)	
ErrorAr = { x : ErrorArAUX[Scenarios.index(x)] for x in Scenarios}

ErrorPros = {}
for n in Prosumers:
	ErrorProsAUX = sample(SampleList, Number_of_Scenarios)	
	ErrorPros.update({ (n,x) : ErrorProsAUX[Scenarios.index(x)] for x in Scenarios})

TestSamples = 10000
TestSet = ['w%d' % x for x in range(1,TestSamples+1)]
ProbTestSet = { x : 1/TestSamples for x in TestSet}
ErrorTestSetAUX = sample(SampleList, TestSamples)	
ErrorTestSet = { x : ErrorTestSetAUX[TestSet.index(x)] for x in TestSet}

		
mu = {}
std = {}

# ERROR
muAUX, stdAUX = norm.fit(SampleList)
mu.update({'Total' : np.round(muAUX,3)})
std.update({'Total' : np.round(stdAUX,3)})

# ERROR
muAUX, stdAUX = norm.fit(list({x : Error[x] for x in Scenarios}.values()))
mu.update({'Error' : np.round(muAUX,3)})
std.update({'Error' : np.round(stdAUX,3)})

# ERRORAr
muAUX, stdAUX = norm.fit(list({x : ErrorAr[x] for x in Scenarios}.values()))
mu.update({'Ar' : np.round(muAUX,3)})
std.update({'Ar' : np.round(stdAUX,3)})

# ERRorPros
for n in Prosumers:
    muAUX, stdAUX = norm.fit(list({x : ErrorPros[n,x] for x in Scenarios}.values()))
    mu.update({n : np.round(muAUX,3)})
    std.update({n : np.round(stdAUX,3)})

# ERRORTESTSET
muAUX, stdAUX = norm.fit(list({x : ErrorTestSet[x] for x in TestSet}.values()))
mu.update({'Test' : np.round(muAUX,3)})
std.update({'Test' : np.round(stdAUX,3)})
		
	
Bounds = ['b1','b2']
Q = {'b1' : 1,
	 'b2' : -1}

ADD = 0.0

HAr = {'b1' : max(ErrorAr.values()) + ADD, # maximum value of forecast error
	   'b2' : -1*min(ErrorAr.values()) + ADD} # minimum value of forecast error

HPros = {}
for n in Prosumers:
    HProsAUX = {(n,'b1') : max({x : ErrorPros[n,x] for x in Scenarios}.values()) + ADD,
                (n,'b2') : -1*min({x : ErrorPros[n,x] for x in Scenarios}.values()) + ADD}
    HPros.update(HProsAUX)	

			
# Plot aggregated CDF
# NS = Number_of_Scenarios
# F = np.arange(1/(NS+1),1/(NS+1)*NS+1/(NS+1),1/(NS+1))
# ErrorList = []
# for key, value in Error.items():
    # ErrorList.append(value)

# x = np.sort(ErrorList)

# fig1 = plt.figure()
# ax1 = fig1.add_subplot(111)
# ax1.plot(x,F,color='k')


# Ambiguity aversion
Rho_Ar = 0
Rho_Pros = {'Pros1' : 0,
		   'Pros2' : 0}
		   

#Beta = 		0.00000001
Beta = 	0.0
Epsilon = 	0.05
#Epsilon = 	0.01

# Spatial Arbitrageur
Cost = 0.5

P_lb = -30
P_ub = 30
		

# Prosumers
Utility = {'Pros1' : 0.6,
		   'Pros2' : 0.7
		   }
		
D_lb = {'Pros1' : 0,
	    'Pros2' : 0
	    }
		
D_ub = {'Pros1' : 10,
	    'Pros2' : 10
	    }

		
# Inelastic load
Delta = 15
 


#######
# SAA #
#######
model_name = 'EconomicDispatch'

ED_Input = 	{'Scenarios' : Scenarios,
			 'Prosumers' : Prosumers,
			 #
			 'Beta' : Beta,
			 #
			 'Cost' : Cost,
			 'Utility' : Utility,
			 #
			 'P_lb' : P_lb,
			 'P_ub' : P_ub,
			 #
			 'D_lb' : D_lb,
			 'D_ub' : D_ub,
			 #
			 'Error' : Error,
			 'Prob' : Prob,
			 'Delta' : Delta}  

# Initialize model and run Optimization
ED = OptProblem(model_name,ED_Input,ED_Settings) 
ED.run_opt()
ED.get_results(ED_Settings)



#######
# DRO #
#######
from func.func import calc_ED_DRO

# heterogeneous ambiguity aversion
Amb = pd.DataFrame(columns=['Rho_Ar','Rho_Pros1','Rho_Pros2'])
Amb = Amb.set_index(['Rho_Ar','Rho_Pros1','Rho_Pros2'])


# Set CASE STUDY
rangeStudyRho = np.round(np.arange(0.0, 1.02, 0.02),2) # ambiguity aversion
#rangeStudyRho = [0.2]


ED_DRO_Input = 	{'Scenarios' : Scenarios,
				 'Prosumers' : Prosumers,
				 #
				 'Beta' : Beta,
				 #
				 'Cost' : Cost,
				 'Utility' : Utility,
				 #
				 'P_lb' : P_lb,
				 'P_ub' : P_ub,
				 #
				 'D_lb' : D_lb,
				 'D_ub' : D_ub,
				 #
				 'Error' : Error,
				 'ErrorAr' : ErrorAr,
				 'ErrorPros' : ErrorPros,
				 #
				 'TestSet' : TestSet,
				 'ErrorTestSet' : ErrorTestSet,
				 'ProbTestSet' : ProbTestSet,
				 #
				 'Bounds' : Bounds,
				 'Q' : Q,
				 'HAr' : HAr,
				 'HPros' : HPros,
				 #
				 'Prob' : Prob,
				 'Delta' : Delta,
				 #
				 'Rho_Ar' : 0,
				 'Rho_Pros' : 0,
				 #
				 'Epsilon' : Epsilon,}
	

# # Homogeneous ambiguity aversion
# model_name = 'EconomicDispatch_DRO'
# How = 'Hom_noAmb'
				 
# Parallel_Results_ED_DRO = Parallel(n_jobs=num_cores)(delayed(calc_ED_DRO)(
							# #
							# ED_DRO_Input,model_name,ED_DRO_Settings,How,ADD,
							# Amb,
							# rangeStudyRho,
							# Rho_Ar,Rho_Pros1,Rho_Pros2) 
							# #
							# for Rho_Ar in [0] for Rho_Pros1 in [0] for Rho_Pros2 in [0])

# Hom_noAmb = pd.concat(Parallel_Results_ED_DRO)
# Hom_noAmb.sort_index(inplace=True)



# Heterogeneous ambiguity aversion
model_name = 'EconomicDispatch_DRO'
How = 'Het_Amb_0'
				 
Parallel_Results_ED_DRO = Parallel(n_jobs=num_cores)(delayed(calc_ED_DRO)(
							#
							ED_DRO_Input,model_name,ED_DRO_Settings,How,ADD,
							Amb,
							rangeStudyRho,
							Rho_Ar=Rho_Aux,Rho_Pros1=Rho_Aux,Rho_Pros2=Rho_Aux)
							#
							for Rho_Aux in rangeStudyRho)

Het_Amb_0 = pd.concat(Parallel_Results_ED_DRO)
Het_Amb_0.sort_index(inplace=True)



# # Heterogeneous ambiguity aversion
# model_name = 'EconomicDispatch_DRO'
# How = 'Het_Amb_1'
				 
# Parallel_Results_ED_DRO = Parallel(n_jobs=num_cores)(delayed(calc_ED_DRO)(
							# #
							# ED_DRO_Input,model_name,ED_DRO_Settings,How,ADD,
							# Amb,
							# rangeStudyRho,
							# Rho_Ar=Rho_Aux,Rho_Pros1=Rho_Aux,Rho_Pros2=Rho_Aux)
							# #
							# for Rho_Aux in rangeStudyRho)

# Het_Amb_1 = pd.concat(Parallel_Results_ED_DRO)
# Het_Amb_1.sort_index(inplace=True)

		
		
# Timing execution
#----------------------------
end = time.time()
#----------------------------

print('\n Solving Time: %d min \n' % (round((end - start)/60,2)) )



################
# Plot results #
################

if plotResults == True:

	# Line Plot
	from func.func import plot_increasing_AmbAver
	from func.func import plot_E
	from func.func import plot_R

	case = ['Hom_noAmb','Hom_Amb','Het_Amb_0','Het_Amb_1']
	costData = [Hom_noAmb, Het_Amb_0, Het_Amb_1]

	for x in range(0,len(case)):
		plot_increasing_AmbAver(case[x],costData[x])
		plot_E(case[x],costData[x])
		plot_R(case[x],costData[x])


	# 3d Plot
	from func.func import cost_3D_plot

	case = ['Het_Amb_0','Het_Amb_1']
	Rho_Ar_Aux = 0.2
	FullcostData = [Het_Amb_0.xs((Rho_Ar_Aux), level=('Rho_Ar')),
					Het_Amb_1.xs((Rho_Ar_Aux), level=('Rho_Ar'))]
	What = 'Cost_Tot'

	for x in range(0,len(case)):
		cost_3D_plot(case[x],FullcostData[x],What)
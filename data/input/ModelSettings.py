''' 
=========================================================================
Script         : Model settings
Author         : Niklas Vepermann
Project        : Economic Dispatch
=========================================================================
'''

#########################
# Economic Dispatch SAA #
#########################
ED_Settings = {
'variables' : 	['p',
				 'd',
				 'alpha_Pros',
				 'alpha_Ar'],
#
'objective' : 	['CostMin_CP_SAA'],
#
'constraints' : ['EQ_balance_E',
				 'EQ_balance_R',
				 #
				 'EQ_d_lb',
				 'EQ_d_ub',
				 #
				 'EQ_p_lb',
				 'EQ_p_ub'],
#
'results' : 	['objective',
				'p',
				'd',
				'alpha_Ar',
				'alpha_Pros',
				#
				'lambda_E',
				'lambda_R'
				],
#				
'eval' :		['ObjPros_SAA',
				 'ObjError_SAA',
				 'ObjAr_SAA'] }



#########################
# Economic Dispatch DRO #
#########################				 
ED_DRO_Settings = 	{
'variables' : 	['p',
				 'd',
				 'alpha_Pros',
				 'alpha_Ar',
				 #
				 'Var_Obj',
				 #
				 'Var_d_lb_DRCC',
				 'Var_d_ub_DRCC',
				 #
				 'Var_p_lb_DRCC',
				 'Var_p_ub_DRCC'],
#
'objective' : 	['CostMin_CP_DRO'],
#
'constraints' : ['EQ_balance_E',
				 'EQ_balance_R',
				 #
				 'EQ_Prosumer_Obj_DRO',
				 'EQ_Arbitrageur_Obj_DRO',
				 #
				 'EQ_d_lb_DRCC',
				 'EQ_d_ub_DRCC',
				 #
				 'EQ_p_lb_DRCC',
				 'EQ_p_ub_DRCC'],
#
'results' : 	['objective',
				'p',
				'd',
				'alpha_Ar',
				'alpha_Pros',
				#
				'lambda_E_DRO',
				'lambda_R_DRO'],
#				
'eval' :		['CP_DRO'] }